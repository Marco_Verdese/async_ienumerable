﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Async.Example1
{
    public class Spinner : MonoBehaviour
    {
        [SerializeField] private Button _animateButton;
        [SerializeField] private Animator _cubeAnimator;
        [SerializeField] private Animator _cylinderAnimator;
        
        public void OnAnimateButtonPressed()
        {
            _animateButton.interactable = false;
            var finishedAnimation = 0;

            Action onFinishAction = () =>
            {
                finishedAnimation++;
                
                if (finishedAnimation == 2)
                {
                    _animateButton.interactable = true;
                }
                //_animateButton.interactable = ++finishedAnimation == 2;
            };
            
            StartCoroutine(AnimateCubeCoroutine(onFinishAction));
            StartCoroutine(AnimateCylinderCoroutine(onFinishAction));

            
            //_cylinderAnimator.SetTrigger("Rotate");
            //Example1();
            //Example2();
        }
        
        private IEnumerator AnimateCubeCoroutine(Action onFinished)
        {
            _cubeAnimator.SetTrigger("Rotate");

            do
            {
                yield return null;
            } while (!_cubeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"));

            onFinished();
        }

        private IEnumerator AnimateCylinderCoroutine(Action onFinished)
        {
            _cylinderAnimator.SetTrigger("Rotate");
            
            do
            {
                yield return null;
            } while (!_cylinderAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
            
            onFinished();
        }
        
        
        
        private async void Example1()
        {
            Debug.Log($"Inizio fc: {Time.frameCount}");
            
            StartCoroutine(MyCoroutine(number =>
            {
                Debug.Log($"{number} fc: {Time.frameCount}");
                Debug.Log($"Fine fc: {Time.frameCount}");
            }));
            
            Debug.Log($"Attendo fc: {Time.frameCount}");

            
            await UniTask.Yield();
            await UniTask.Yield();
            Debug.Log($"-----------------");
            
            Debug.Log($"Inizio fc: {Time.frameCount}");
            Debug.Log($"Attendo fc: {Time.frameCount}");
            
            var number2 = await MyTask();
            
            Debug.Log($"{number2} fc: {Time.frameCount}");
            Debug.Log($"Fine fc: {Time.frameCount}");
        }

        private int[] _array = {1, 2};
        
        private void Example2()
        {
            Debug.Log("Inizio");
            try
            {
                //throw new ArgumentException();
                Debug.Log(_array[2]);

                Debug.Log("Faccio cose");
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.Log("C'è stato un errore di index.");
            }
            catch (Exception e)
            {
                Debug.Log("C'è stato un errore generico.");
            }
            finally
            {
                Debug.Log("Termino");
            }
        }

        private IEnumerator MyCoroutine(Action<int> onFinished)
        {
            Debug.Log($"Attendo 2 fc: {Time.frameCount}");
            
            // Questa linea è inamovibile
            yield return null; // Aspetta un frame - METTO IN PAUSA
            
            Debug.Log($"Attendo 3 fc: {Time.frameCount}");

            onFinished(5);
        }

        private async UniTask<int> MyTask()
        {
            Debug.Log($"Attendo 2 fc: {Time.frameCount}");
            
            await UniTask.Yield(); // Aspetta un frame
            
            Debug.Log($"Attendo 3 fc: {Time.frameCount}");
            
            return 5;
        }
    }
}