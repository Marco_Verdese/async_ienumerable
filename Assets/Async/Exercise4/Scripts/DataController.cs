using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace EH.Async.Excercise4
{
    public class DataController : MonoBehaviour
    {
        readonly string USERS_URL = "https://jsonplaceholder.typicode.com/users";
        readonly string TODOS_URL = "https://jsonplaceholder.typicode.com/todos";


        private void Start()
        {
        }
    
        private async UniTask<Users> FetchUsers()
        {
            return new Users();
        }

        private async UniTask<ToDos> FetchToDos()
        {
            return new ToDos();
        }
        

        [Serializable]
        public struct Users
        {
            public User[] users;
        }


        [Serializable]
        public struct User
        {
            public string name;
            public string username;
            public Address address;
            
            [Serializable]
            public struct Address
            {
                public string street;
                public Geo geo;
                
                [Serializable]
                public struct Geo
                {
                    public string lat;
                    public string lng;
                }
            }
        }
        
        
        [Serializable]
        public struct ToDos
        {
            public ToDo [] toDos;
        }
        
        [Serializable]
        public struct ToDo
        {
            public int userId;
            public int id;
            public string title;
            public string completed;
        }
    }
}