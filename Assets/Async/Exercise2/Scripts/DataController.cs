using System;
using System.Collections;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Async.Excercise2
{
    public class DataController : MonoBehaviour
    {
        readonly string USERS_URL = "https://jsonplaceholder.typicode.com/users";
        readonly string TODOS_URL = "https://jsonplaceholder.typicode.com/todos";

        private IEnumerator Start()
        {
            Debug.Log("Inizio");
            yield return StartCoroutine(FetchDataCoroutine());
            Debug.Log("Finisco");
            // TODO: download Users -> parse users
            // TODO: download Todo -> parse todo
            // TODO: print Debug.Log($"{user.name} - {user.username} ({user.address.geo.lat}, {user.address.geo.lng})”); (si possono parser solo I dati che servono)
            // TODO: print Debug.Log($"{todo.id}: {todo.userId}, {todo.title}, {todo.completed}");
            // In caso di errore deve bloccarsi l'esecuzione (yield break)
            // Possiamo far sapere a Start che c’è stata un’eccezione/problema e fargli fare qualcosa
        }
        
        private IEnumerator FetchDataCoroutine(Action<string> onError = null)
        {
            using (WWW www = new WWW(USERS_URL))
            {
                yield return www;
                Debug.Log(www.text);
                
                var users = JsonUtility.FromJson<Users>("{ \"users\" :" + www.text + "}");
                
                foreach (var user in users.users)
                {
                    Debug.Log($"{user.name} - {user.username} ({user.address.geo.lat}, {user.address.geo.lng})");
                }
            }

            using (WWW www = new WWW(TODOS_URL))
            {
                yield return www;
                
                var todos = JsonUtility.FromJson<TODOS>("{ \"todo\" :" + www.text + "}");
                
                foreach (var toDo in todos.todo)
                {
                    Debug.Log($"{toDo.id}: {toDo.userId}, {toDo.title}, {toDo.completed}");
                }
            }
            yield break;
        }
        
        public struct Users
        {
            public User[] users;
        }
        
        public struct TODOS
        {
            public ToDo[] todo;
        }

        [Serializable]
        public struct User
        {
            public string name;
            public string username;
            public Address address;
            
            [Serializable]
            public struct Address
            {
                public string street;
                public Geo geo;
                
                [Serializable]
                public struct Geo
                {
                    public string lat;
                    public string lng;
                }
            }
        }
        
        [Serializable]
        public struct ToDo
        {
            public int userId;
            public int id;
            public string title;
            public string completed;
        }

        
        
    }
    
}