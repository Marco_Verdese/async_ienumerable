﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using UnityEngine;

namespace Async.Exercise1
{
    public class CharacterCount : MonoBehaviour
    {
        private static readonly string URL = "https://docs.microsoft.com/en-us/dotnet/csharp/csharp";

        private void Start()
        {
            CalculatePageCharacterCount ();
        }

        private void CalculatePageCharacterCount ()
        {
            // Debug.Log che sta inziando
            Debug.Log("Sto inizando");        
            StartCoroutine(DownloadText((text) =>
                {
                    Debug.Log(text.Length);                            //Inizia la coroutine dove andrà a connettersi al sito per scaricare il testo
                }
                ));
            Debug.Log("Attendo i risultati");
           
            
            // Questo metodo deve ricevere il testo quando chi se ne doveva occupare ha finito
            // Stampare che sta attendendo i risultati
            // Una volta ricevuti stampare la lunghezza della pagina (numero di caratteri)
        }

        // void OnFinish(string) 
        // string OnFinish()
        // Action -> Metodo void che prende in ingresso cosa_
        // Func -> Meodo di che tipo_ Cosa restituisce e cosa prendere in ingresso
        private IEnumerator DownloadText(Action<string> onFinish) //Coroutine che prende in ingresso una Lamba(=>) che a sua volta prende in ingresso una "string"
        {
            using(WWW www = new WWW(URL))
            {
                yield return www;        //Aspetta i risultati dal WEB(non salto come al solito un frame, ma dovrò aspettare in base alla mia connessione)
                onFinish(www.text);
                
            }
        }
        
    }

}
