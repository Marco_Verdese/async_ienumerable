﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.CompilerServices;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Async.Exercise3
{
    public class CharacterCount : MonoBehaviour
    {
        private static readonly string URL = "https://docs.microsoft.com/en-us/dotnet/csharp/csharp";
        
        private void Start()
        {
            CalculatePageCharacterCount ();
        }
        
        private void CalculatePageCharacterCount ()
        {
            // Debug.Log che sta inziando
            // Someone should download the text asynchronously (tip: HttpClient)
            // Print the results after who should download them has finished
            // The results should include print how many characters are present in the url
        }
    }
}