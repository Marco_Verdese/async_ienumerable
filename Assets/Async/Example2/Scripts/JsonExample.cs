﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Async.Example2
{
    public class JsonExample : MonoBehaviour
    {
        private void Start()
        {
            var jsonMario = "{\"name\": \"Mario\", \"surname\": \"Rossi\", \"favoriteNumber\": 14,\"languages\":[ \"it\", \"en\"] }";
            Debug.Log(jsonMario);
            
            var rocco = new User("Rocco Luigi", "Tartaglia", 42, new []{"it", "en", "jp"});
            Debug.Log(rocco);
        }
        
        
        [Serializable]
        public struct User
        {
            public string name;
            public string surname;
            public int favoriteNumber;
            public string[] languages;

            public User(string name, string surname, int favoriteNumber, string[] languages)
            {
                this.name = name;
                this.surname = surname;
                this.favoriteNumber = favoriteNumber;
                this.languages = languages;
            }

            public override string ToString()
            {
                //Debug.Log(languages.Aggregate("valoreIniziale", (elementoPrecedente, elementoAttuale) => elementoPrecedente + elementoAttuale));
                return $"Nome: {name}\nCognome: {surname}\nNumero preferito: {favoriteNumber}\nLingue conosciute: {languages.Aggregate((input, currentLanguage) => $"{input}, {currentLanguage}")}";
            }
        }
    }
}
