﻿#define EXERCISE_1
#define EXERCISE_2
#define EXERCISE_3
#define EXERCISE_4
#define EXERCISE_5
#define EXERCISE_6
#define EXERCISE_EXTRA

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;


namespace LinqTests
{
#if EXERCISE_1
    public class LinqTestsExercise1
    {
        [Test]
        public void FirstSingleLetterWord()
        {
            var answer = LinqExercise.GetFirstSingleLetterWord(new List<string> {"This", "is", "a", "test"});
            Assert.AreEqual("a", answer);

            answer = LinqExercise.GetFirstSingleLetterWord(new List<string> {"I", "am", "not", "a", "crook"});
            Assert.AreEqual("I", answer);

            answer = LinqExercise.GetFirstSingleLetterWord("deadbeef".ToCharArray().Select(_ => _.ToString()));
            Assert.AreEqual("d", answer);
        }

        [Test]
        public void LastWordWithHerInIt()
        {
            var answer = LinqExercise.GetLastWordWithHerInIt("Watching the weather together with her".Split(' '));
            Assert.AreEqual("her", answer);

            answer = LinqExercise.GetLastWordWithHerInIt("Watching the watcher alone".Split(' '));
            Assert.AreEqual("watcher", answer);

            answer = LinqExercise.GetLastWordWithHerInIt("where where where where".Split(' '));
            Assert.AreEqual("where", answer);
        }

        [Test]
        public void FifthWordIfItExists()
        {
            var answer = LinqExercise.GetFifthWordIfItExists("Watching the weather together with her".Split(' '));
            Assert.AreEqual("with", answer);

            answer = LinqExercise.GetFifthWordIfItExists("Watching the watcher alone".Split(' '));
            Assert.AreEqual(null, answer);

            answer = LinqExercise.GetFifthWordIfItExists(new List<string>());
            Assert.AreEqual(null, answer);
        }

        [Test]
        public void GetLastWordIfAny()
        {
            var answer = LinqExercise.GetLastWordIfAny("Watching the weather together with her".Split(' '));
            Assert.AreEqual("her", answer);

            answer = LinqExercise.GetLastWordIfAny("Watching the watcher alone".Split(' '));
            Assert.AreEqual("alone", answer);

            answer = LinqExercise.GetLastWordIfAny(new List<string>());
            Assert.AreEqual(null, answer);
        }
    }
#endif
#if EXERCISE_2
    public class LinqTestsExercise2
    {
         [Test]
        public void ThirdFourthFifthItems()
        {
            var words = "This is the first test".Split(' ');
           
            var answer = LinqExercise.GetThirdFourthFifthItems(words);
            var str = string.Join(", ", answer);
            Assert.AreEqual("the, first, test", str);

            words = "I'm sorry Dave I can't do that".Split(' ');
            answer = LinqExercise.GetThirdFourthFifthItems(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("Dave, I, can't", str);

            words = "640K should be enough for anybody".Split(' ');
            answer = LinqExercise.GetThirdFourthFifthItems(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("be, enough, for", str);

        }

        [Test]
        public void StartThroughEnd()
        {
            var words = "This start is the end first test".Split(' ');
            var answer = LinqExercise.GetStartThroughEnd(words);
            var str = string.Join(", ", answer);
            Assert.AreEqual("start, is, the", str);

            words = "start I'm sorry Dave end I can't do that".Split(' ');
            answer = LinqExercise.GetStartThroughEnd(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("start, I'm, sorry, Dave", str);

            words = "640K should be start end enough for anybody".Split(' ');
            answer = LinqExercise.GetStartThroughEnd(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("start", str);

        }


        [Test]
        public void DistinctShortWords()
        {
            var words = "Cant see the forest for the trees".Split(' ');
            var answer = LinqExercise.GetDistinctShortWords(words);
            var str = string.Join(", ", answer);
            Assert.AreEqual("see, the, for", str);

            words = "Im sorry Dave I cant do that".Split(' ');
            answer = LinqExercise.GetDistinctShortWords(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("Im, I, do", str);

            words = "it was the best of times it was the worst of times".Split(' ');
            answer = LinqExercise.GetDistinctShortWords(words);
            str = string.Join(", ", answer);
            Assert.AreEqual("it, was, the, of", str);

        }
    }
#endif
#if EXERCISE_3
    public class LinqTestsExercise3
    {
        [Test]
        public void SortNamesByLast()
        {
            var names = new List<LinqExercise.Name>
            {
                new NameImpl("Martin Luther King"),
                new NameImpl("Johan Sebastian Bach"),
                new NameImpl("Wolfgang Amadeus Mozart"),
                new NameImpl("Franklin Delano Roosevelt")
            };

            var answer = LinqExercise.SortNamesByLast(names);
            var str = string.Join(", ", answer.Select(_ => _.Last));
            Assert.AreEqual("Roosevelt, Mozart, King, Bach", str);

            names = new List<LinqExercise.Name>
            {
                new NameImpl("Hillary Rodham Clinton"),
                new NameImpl("Edgar Allan Poe"),
                new NameImpl("Billie Jean King"),
                new NameImpl("John Fitzgerald Kennedy")
            };

            answer = LinqExercise.SortNamesByLast(names);
            str = string.Join(", ", answer.Select(_ => _.Last));
            Assert.AreEqual("Poe, King, Kennedy, Clinton", str);
        }

        [Test]
        public void SortNamesByLastFirstMiddle()
        {
            var names = new List<LinqExercise.Name>
            {
                new NameImpl2("Johan Sebastian Bach"),
                new NameImpl2("Martin Luther King"),
                new NameImpl2("Billie Jean King"),
                new NameImpl2("Im The King"),
                new NameImpl2("Franklin Delano Roosevelt")
            };
          
            var answer = LinqExercise.SortNamesByLastFirstMiddle(names);
            var str = string.Join(", ", answer.Select(_ => _.First));
            Assert.AreEqual("Johan, Billie, Im, Martin, Franklin", str);

            names = new List<LinqExercise.Name>
            {
                new NameImpl2("Hillary Rodham Clinton"),
                new NameImpl2("Edgar Ellen Poe"),
                new NameImpl2("Edgar Allan Poe"),
                new NameImpl2("John Fitzgerald Kennedy")
            };

            answer = LinqExercise.SortNamesByLastFirstMiddle(names);
            str = string.Join(", ", answer.Select(_ => _.Middle));
            Assert.AreEqual("Rodham, Fitzgerald, Allan, Ellen", str);
        }

        private class NameImpl : LinqExercise.Name
        {
            public string First { get; }

            public string Middle { get; }

            public string Last { get; }

            public NameImpl(string name)
            {
                var names = name.Split(' ');
                First = names[0];
                Middle = names[1];
                Last = names[2];
            }

            public override string ToString()
            {
                return $"{First} {Middle} {Last}";
            }
        }
        private class NameImpl2 : LinqExercise.Name
        {
            public string First { get; }

            public string Middle { get; }

            public string Last { get; }

            public NameImpl2(string name)
            {
                var names = name.Split(' ');
                First = names[0];
                Middle = names[1];
                Last = names[2];
            }

            public override string ToString()
            {
                return $"{First} {Middle} {Last}";
            }
        }
    }
#endif
#if EXERCISE_4
    public class LinqTestsExercise4
    {
        [Test]
        public void NumberThatStartWith()
        {
            var words = "This is the first test".Split(' ');
            var answer = LinqExercise.NumberThatStartWith(words, "is");
            Assert.AreEqual("1", answer.ToString());

            words = "I'm sorry Dave I can't do that".Split(' ');
            answer = LinqExercise.NumberThatStartWith(words, "I");
            Assert.AreEqual("2", answer.ToString());

            words = "She sells sea shells down by the sea shore".Split(' ');
            answer = LinqExercise.NumberThatStartWith(words, "se");
            Assert.AreEqual("3", answer.ToString());

        }

        [Test]
        public void LengthOfShortestWord()
        {
            var words = "This is the first test".Split(' ');
            var answer = LinqExercise.LengthOfShortestWord(words);
            Assert.AreEqual("2", answer.ToString());

            words = "I'm sorry Dave I can't do that".Split(' ');
            answer = LinqExercise.LengthOfShortestWord(words);
            Assert.AreEqual("1", answer.ToString());

            words = "She sells sea shells down by the sea shore".Split(' ');
            answer = LinqExercise.LengthOfShortestWord(words);
            Assert.AreEqual("2", answer.ToString());

        }

        [Test]
        public void TotalCharactersInSequence()
        {
            var words = "This is the first test".Split(' ');
            var answer = LinqExercise.TotalCharactersInSequence(words);
            Assert.AreEqual("18", answer.ToString());

            words = "I'm sorry Dave I can't do that".Split(' ');
            answer = LinqExercise.TotalCharactersInSequence(words);
            Assert.AreEqual("24", answer.ToString());

            words = "She sells sea shells down by the sea shore".Split(' ');
            answer = LinqExercise.TotalCharactersInSequence(words);
            Assert.AreEqual("34", answer.ToString());

        }
    }
#endif
#if EXERCISE_5
    public class LinqTestsExercise5
    {
        [Test]
        public void DisplayStringsForNames()
        {
            var names = new List<LinqExercise.Name>
            {
                new NameImpl("Martin Luther King"),
                new NameImpl("Johan Sebastian Bach"),
                new NameImpl("Wolfgang Amadeus Mozart"),
                new NameImpl("Franklin Delano Roosevelt")
            };
            var description = "{" + string.Join(", ", names) + "}";
            var answer = LinqExercise.DisplayStringsForNames(names);
            var str = string.Join("; ", answer);
            Assert.AreEqual("King, Martin; Bach, Johan; Mozart, Wolfgang; Roosevelt, Franklin", str);

            names = new List<LinqExercise.Name>
            {
                new NameImpl("Hillary Rodham Clinton"),
                new NameImpl("Edgar Allan Poe"),
                new NameImpl("Billie Jean King"),
                new NameImpl("John Fitzgerald Kennedy")
            };
            description = "{" + string.Join(", ", names) + "}";
            answer = LinqExercise.DisplayStringsForNames(names);
            str = string.Join("; ", answer);
            Assert.AreEqual("Clinton, Hillary; Poe, Edgar; King, Billie; Kennedy, John", str);

        }

        private class NameImpl : LinqExercise.Name
        {
            public string First { get; }

            public string Middle { get; }

            public string Last { get; }

            public NameImpl(string name)
            {
                var names = name.Split(' ');
                First = names[0];
                Middle = names[1];
                Last = names[2];
            }

            public override string ToString()
            {
                return $"{First} {Middle} {Last}";
            }
        }
    }
#endif
#if EXERCISE_6
    public class LinqTestsExercise6
    {
        [Test]
        public void TestForSquares()
        {
            var answer = LinqExercise.TestForSquares(new[] { 1, 2, 3, 4, 5 }, new[] { 1, 4, 9, 16, 25 });
            Assert.AreEqual(true, answer);

            answer = LinqExercise.TestForSquares(new[] { 1, 2, 5 }, new[] { 1, 4, 9 });
            Assert.AreEqual(false, answer);

            answer = LinqExercise.TestForSquares(new[] { 12 }, new[] { 144 });
            Assert.AreEqual(true, answer);

            answer = LinqExercise.TestForSquares(new[] { 1, 5, 3 }, new[] { 1, 25, 9, 9 });
            Assert.AreEqual(false, answer);

            answer = LinqExercise.TestForSquares(new int[0], new int[0]);
            Assert.AreEqual(true, answer);

        }

        [Test]
        public void GetTheLastWord()
        {
            var answer = LinqExercise.GetTheLastWord(new[] { "he", "she", "it", "we", "you", "they" });
            Assert.AreEqual("The last word is we", answer);

            answer = LinqExercise.GetTheLastWord(new[] { "hop", "top", "stop", "cop", "lop", "chop" });
            Assert.AreEqual(null, answer);

            answer = LinqExercise.GetTheLastWord(new[] { "elastic", "elaborate", "elephant", "iris", "ibis", "incredible" });
            Assert.AreEqual("The last word is incredible", answer);

            answer = LinqExercise.GetTheLastWord(new string[0]);
            Assert.AreEqual(null, answer);

        }
    }
#endif
#if EXERCISE_EXTRA
    public class LinqTestsExerciseExtra
    {
        [Test]
        public void TransformTest()
        {
            IEnumerable<int> enumerable = new List<int> { 10, 20, 30 }.Transform(x => x / 10);
            var answer = string.Join(" ", enumerable);
            Assert.AreEqual("1 2 3", answer);

            enumerable = new List<int> { 7, 11, 15, 19 }.Transform(x => x - 7);
            answer = string.Join(" ", enumerable);
            Assert.AreEqual("0 4 8 12", answer);

            enumerable = new List<int> { 1, 1, 1, 2, 2, 2 }.Transform(x => 20);
            answer = string.Join(" ", enumerable);
            Assert.AreEqual("20 20 20 20 20 20", answer);

            enumerable = new List<int> { 1, 2, 3 }.Transform(x => x * 2);
            answer = string.Join(" ", enumerable);
            Assert.AreEqual("2 4 6", answer);

        }
    }
#endif
}