﻿#define EXERCISE_1
#define EXERCISE_2

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using UnityEngine;
using UnityEngine.TestTools;


namespace EnumerableTests
{
    public class EnumerableTests
    {
#if EXERCISE_1    
        // A Test behaves as an ordinary method
        [Test]
        public void EnumerableTestsAddTwoThenDouble()
        {
            // Use the Assert class to test conditions
            Assert.AreEqual(new List<int>{2, 4, 6, 12, 14, 28, 30, 60, 62, 124}, EnumeratorExercise.AddTwoThenDouble().Take(10));
            Assert.AreEqual(new List<int>{508, 510, 1020, 1022}, EnumeratorExercise.AddTwoThenDouble().Take(17).Skip(13));
        }
#endif
#if EXERCISE_2    
        [Test]
        public void EnumerableTestsAddTwoThenDoubleWithParameter()
        {
            // Use the Assert class to test conditions
            Assert.AreEqual(new List<int>{2, 4, 6, 12, 14, 28, 30, 60, 62, 124}, EnumeratorExercise.AddTwoThenDouble(10));
            Assert.AreEqual(new List<int>{508, 510, 1020, 1022}, EnumeratorExercise.AddTwoThenDouble(17).Skip(13));
        }
#endif
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        // [UnityTest]
        // public IEnumerator EnumerableTestsAddTwoThenDoubleWithParameter()
        // {
        //     // Use the Assert class to test conditions.
        //     // Use yield to skip a frame.
        //     yield return null;
        // }
    }
}