﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LinqExercise : MonoBehaviour
{
    /************ EXERCISE 1 ************/

    // Return the first word with just one letter in it, out of a sequence of words.
    // There will always be at least one.
    public static string GetFirstSingleLetterWord(IEnumerable<string> words)
    {
        return words.First(word => word.Length == 1);
        //return words.First(ControllaCheSiaLungoUno);
    }

    // Return the last word that contains the substring "her" in it.
    // There will always be at least one.
    public static string GetLastWordWithHerInIt(IEnumerable<string> words)
    {
        return null;
    }

    // Return the fifth word in the sequence, if there is one. If there are
    // fewer than 5 words, then return null.
    public static string GetFifthWordIfItExists(IEnumerable<string> words)
    {
        return null;
    }

    // Return the last word in the sequence. If there are no words in
    // the sequence, return null.
    public static string GetLastWordIfAny(IEnumerable<string> words)
    {
        return null;
    }


    /************ EXERCISE 2 ************/
    // Return the 3rd, 4th and 5th item of the list
    public static IEnumerable<string> GetThirdFourthFifthItems(IEnumerable<string> words)
    {
        return null;
    }
    
    // Return all words in the sequence between "start" (inclusive)
    // and "end" (non-inclusive)
    // For example, if given { "One", "start", "more", "end", "thing" } ...
    // this method should return { "start", "more" }
    public static IEnumerable<string> GetStartThroughEnd(IEnumerable<string> words)
    {
        return null;
    }
    
    // Return all distinct words that have less than four letters in them.
    public static IEnumerable<string> GetDistinctShortWords(IEnumerable<string> words)
    {
        return null;
    }
    
    /************ EXERCISE 3 ************/
    
    public interface Name
    {
        string First { get; }
        string Middle { get; }
        string Last { get; }
    }

    // Return the provided list of names, ordered by Last, in
    // descending order.
    public static IEnumerable<Name> SortNamesByLast(IEnumerable<Name> names)
    {
        return null;
    }
        
    // Return the provided list of names, ordered by Last, then
    // First, then Middle
    public static IEnumerable<Name> SortNamesByLastFirstMiddle(IEnumerable<Name> names)
    {
        return null;
    }
    
    
    /************ EXERCISE 4 ************/

    // Return the number of strings in the provided sequence that begin with
    // the provided startString.
    public static int NumberThatStartWith(IEnumerable<string> words, string startString)
    {
        return Int32.MaxValue;
    }

    // Return the length of the shortest word
    public static int LengthOfShortestWord(IEnumerable<string> words)
    {
        return Int32.MaxValue;
    }
    
    // Return the totxal number of characters in all words in
    // the source sequence
    public static int TotalCharactersInSequence(IEnumerable<string> words)
    {
        return Int32.MaxValue;
    }
    
    /************ EXERCISE 5 ************/
    // Return display strings in the form of "<Last>, <First>" for
    // each provided name
    public static IEnumerable<string> DisplayStringsForNames(IEnumerable<Name> names)
    {
        return null;
    }
    
    /************ EXERCISE 6 ************/
    
    // The following method should return true if each element in the squares sequence
    // is equal to the square of the corresponding element in the numbers sequence.
    // Try to write the entire method using only LINQ method calls, and without writing
    // any loops.
    public static bool TestForSquares(IEnumerable<int> numbers, IEnumerable<int> squares)
    {
        return false;
    }
    
    // Given a sequence of words, get rid of any that don't have the character 'e' in them,
    // then sort the remaining words alphabetically, then return the following phrase using
    // only the final word in the resulting sequence:
    //    -> "The last word is <word>"
    // If there are no words with the character 'e' in them, then return null.
    //
    // TRY to do it all using only LINQ statements. No loops or if statements.
    public static string GetTheLastWord(IEnumerable<string> words)
    {
        return null;
    }

    // Delegate
    public delegate int TransformFunction(int item);
    // intList.Transform(num => num * 2);
    // intList.Select(num => num * 2);
}


// Write the extension method (and containing class) here.
// The method should be called Transform(), extend IEnumerable<int>,
// take an extra parameter of type TransformationFunction, and
// return an IEnumerable<int>.

// Dovrà reimplementare Select, applica cioé TransformFunction a OGNI elemento della lista


// Extension di linq
public static class LinqExtension
{
    public static IEnumerable<int> Transform(this IEnumerable<int> enumerator, LinqExercise.TransformFunction transformFunction)
    {
        return null;
    }
}