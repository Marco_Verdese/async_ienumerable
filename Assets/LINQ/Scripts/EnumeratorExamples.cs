using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EH.LINQ.Examples
{
    public class EnumeratorExamples : MonoBehaviour
    {
        private List<int> fibonacci = new List<int>();
        
        private IEnumerable<int> GetFibonacci()
        {
            var previousValue1 = 0;
            var previousValue2 = 1;

            while (true)
            {
                var nextValue = previousValue1 + previousValue2;
                previousValue1 = previousValue2;
                previousValue2 = nextValue;
                yield return nextValue;
            }
        }

        private IEnumerator BasicEnumerator()
        {
            int i = 0;
            do
            {
                yield return i;
            } while (i++ < 5);
        }
        
        private IEnumerator<int> BasicEnumeratorWithSpecificReturnTyper()
        {
            int i = 0;
            do
            {
                yield return i;
            } while (i++ < 5);
        }
        
        private void Start()
        {
            var i = 0;
            foreach (var value in GetFibonacci())
            {
                Debug.Log(value);
                
                if(i++ > 4) break;
            }
            
            // foreach (var value in GetFibonacci().Take(5))
            // {
            //     Debug.Log(value);
            // }
            
            // var basicEnumerator = BasicEnumerator();
            // while (basicEnumerator.MoveNext())
            // {
            //     Debug.Log(basicEnumerator.Current);
            // }
            
            var basicEnumerator = BasicEnumeratorWithSpecificReturnTyper();
            while (basicEnumerator.MoveNext())
            {
                Debug.Log(basicEnumerator.Current);
            }
        }
    }
}